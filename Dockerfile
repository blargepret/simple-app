# Create the image from a base Bash image
FROM busybox

# Import and configure the "Hello World" script
ARG BASEDIR="/home/helloworld"
ENV WELCOME_MESSAGE="Hello " \
    PATH="${PATH}:${BASEDIR}"
RUN adduser -D -h ${BASEDIR} helloworld
COPY --chown=helloworld:nogroup hello_world.sh ${BASEDIR}/hello_world.sh

# Prepare container startup
USER helloworld
WORKDIR ${BASEDIR}
ENTRYPOINT ["/bin/sh", "-C", "hello_world.sh"]
#ENTRYPOINT ["/bin/sh", "-c", "echo ${WELCOME_MESSAGE}"]