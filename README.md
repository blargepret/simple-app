# Simple app

Sample NodeJS/Angular application for demo.

# Hello world on Docker

```Dockerfile
# Create the image from a base Bash image
FROM busybox

# Import and configure the "Hello World" script
ARG BASEDIR="/home/helloworld"
ENV WELCOME_MESSAGE="Hello " \
    PATH="${PATH}:${BASEDIR}"
RUN adduser -D -h ${BASEDIR} helloworld
COPY --chown=helloworld:nogroup hello_world.sh ${BASEDIR}/hello_world.sh

# Prepare container startup
USER helloworld
WORKDIR ${BASEDIR}
ENTRYPOINT ["/bin/sh", "-C", "hello_world.sh"]
```

```sh
#!/bin/sh
echo "${WELCOME_MESSAGE}"
```

# Initialize cluster on https://labs.play-with-k8s.com

1) Start master node and activate network capabilities

kubeadm init --apiserver-advertise-address $(hostname -i)
kubectl apply -n kube-system -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 |tr -d '\n')"

2) Give elevated access rights to un-authentified nodes on master node

kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous

3) Start worker node, the command with the right parameters is given in the output logs of the master node startup

kubeadm join ip_address --token xxx --discovery-token-ca-cert-hash sha256:xxx

4) Propagate .kube config from the master node to the worker node, copy the file ~/.kube/config
