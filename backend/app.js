var fs = require("fs");
var express = require("express");
var bodyParser = require("body-parser");
var mongo = require("mongodb");
var cors = require("cors");

var server = express();
server.use(bodyParser({extended: false}));
server.use(cors({
  'allowedHeaders': ['sessionId', 'Content-Type'],
  'exposedHeaders': ['sessionId'],
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}));


const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://" + process.env.MONGODB_URL;  // "mongodb://172.27.240.1:27017";
const databaseName = process.env.MONGODB_DB;  // "simpleapp";
const collectionName = "contacts";
var mongo = function(callback) {
	MongoClient.connect(url, function(err, client) {
		if (!err) {
			db = client.db(databaseName);
			collection = db.collection(collectionName);
			callback(collection);
		    client.close();
		} else {
			console.log(err);
		}
	})
};

console.log("Test database connection");
mongo(function(collection) {
    console.log("Database available");
});

var generateAvatar = function() {
    return "abcd";
};

server.post("/contact/add", function(request, response) {
    console.log("Calling [add contact].");
    
    var name = request.body.name;
    var email = request.body.email;
    var phone = request.body.phone;
    var country = request.body.country;
    var isVerified = request.body.isVerified;
    
    if (name) name = name.trim();
    if (email) email = email.trim();
    if (phone) phone = phone.trim();
    if (country) country = country.trim();
    console.log("name=" + name + ",email=" + email + ",phone=" + phone + ",country=" + country + ",isVerified=" + isVerified);
    
    if (!name) {
        response.send(JSON.stringify({error: "MISSING_NAME"}));
    } else if (!email && !phone) {
        response.send(JSON.stringify({error: "MISSING_CONTACT"}));
    } else {
		mongo(function(collection) {
			collection.insertOne({name: name, email: email, phone: phone, country: country, isVerified: isVerified, avatar: generateAvatar()});
		});
        console.log("Contact saved.");
        response.send("OK");
    }
});

server.post("/contact/delete", function(request, response) {
    console.log("Calling [delete contact].");
    
    var email = request.body.email;
    if (email) email = email.trim();
    console.log("email=" + email);
    
    if (!email) {
        response.send(JSON.stringify({error: "MISSING_EMAIL"}));
    } else {
		mongo(function(collection) {
			collection.deleteOne({email: email}, function(err, result) {
				if (!err) {
					if (result.result.n) {
						console.log("Contact deleted");
						response.send("OK");
					} else {
						response.send(JSON.stringify({error: "NOT_EXIST"}));
					}
				} else {
					console.log(err);
					response.send("NOK");
				}
			});
		})
    }
});

// TODO: TRY CATCH ON ALL OPERATIONS TO PREVENT SERVER FROM STOPPING
server.post("/contact/list", function(request, response) {
    console.log("Calling [list contacts].");
	mongo(function(collection) {
		collection.find({}).toArray(function(err, data) {
			if (!err) {
				response.send(JSON.stringify({contacts: data}));
			} else {
				console.log(err);
				response.send("NOK");
			}
		});
	});
});

server.listen(8080);
