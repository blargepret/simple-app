let res = [
  db.createCollection("contacts"),
  db.contacts.insertMany([
    {
	  name: "Alain Proviste",
	  email: "aproviste@gmail.com",
	  phone: "010982312",
	  country: "Belgium",
	  isVerified: false,
	  avatar: ""
	},
	{
	  name: "Valentin Cognito",
	  email: "valentin.cognito@gmail.com",
	  phone: "010912332",
	  country: "Belgium",
	  isVerified: false,
	  avatar: ""
	},
	{
	  name: "Jean Douille",
	  email: "jd@yahoo.com",
	  phone: "010322112",
	  country: "France",
	  isVerified: false,
	  avatar: ""
	}
  ])
]
printjson(res)
