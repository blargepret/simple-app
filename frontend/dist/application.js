angular
    .module("demo")
    .config(["$routeProvider", "$mdThemingProvider", function($routeProvider, $mdThemingProvider) {
        $routeProvider.when("/contacts", {templateUrl: "contact/dashboard.html", controller: "contactController"})
        .otherwise("/contacts");
        
        $mdThemingProvider.theme('default')
        .primaryPalette('light-green')
    }])
    .service("$api", ["$http", "$location", function($http, $location) {
		// Special case for Katacoda
		var protocol = $location.protocol();
		var host = $location.host();
        var servicesEndPoint = protocol + "://" + host + ":" + ($location.port() + 1) + "/";
		if (host.includes("katacoda.com")) {
			if (host.includes("-")) {
				var baseport = host.split("-")[1];
				host = host.replace("-" + baseport + "-", "-" + (parseInt(baseport) + 1) + "-");
				servicesEndPoint = protocol + "://" + host + "/"; 
			}
		}
        return {
            addContact: function(contact, callback) {
                $http.post(servicesEndPoint + "contact/add", contact).then(function(response) {
                    callback(response.data.error);
                }, function(response) {
                    alert(response.status);
                });
            },
            deleteContact: function(email, callback) {
                $http.post(servicesEndPoint + "contact/delete", {email: email}).then(function(response) {
                    callback(response.data.error);
                }, function(response) {
                    alert(response.status);
                });
            },
            getContacts: function(callback) {
                $http.post(servicesEndPoint + "contact/list", {}).then(function(response) {
                    callback(response.data.error, response.data.contacts);
                }, function(response) {
                    alert(response.status);
                });
            }
        };
    }])
    .directive("demoMessage", function() {
        return {
            require: ["message"],
            scope: {
                message: "=message"
            },
            restrict: "E",
            template: "<div class='errorMessage' ng-show='message && message.length > 0'>{{message}}<div>"
        };
    })
    .filter("isContactVerified", [function() {
        return function(input, isVerified) {
            var filtered = [];
            for (var index in input) {
                var row = input[index];
                if (!isVerified || (isVerified && row.isVerified == isVerified)) {
                    filtered.push(row);
                }
            }
            return filtered;
        };
    }]);