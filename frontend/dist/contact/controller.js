angular
    .module("demo")
    .controller("contactController", ["$scope", "$api", "$mdDialog", function($scope, $api, $mdDialog) {
        $scope.reset = function() {
            $scope.name = "";
            $scope.email = "";
            $scope.phone = "";
            $scope.country = "";
            $scope.isVerified = false;
        };
       
        $scope.addContact = function() {
            $scope.message = "";
            var contact = {name: $scope.name, email: $scope.email, phone: $scope.phone, country: $scope.country, isVerified: $scope.isVerified};
            $api.addContact(contact, function(code) {
                if (code) {
                    $scope.message = code;
                } else {
                    $scope.contacts.push(contact);
                    $scope.reset();
                }
            });
        };
        
        $scope.loadContacts = function() {
            $api.getContacts(function(code, contacts) {
                $scope.contacts = contacts;
            });
        };
      
        $scope.switchLanguage = function() {
        };
       
        $scope.delete = function(contact) {
			$api.deleteContact(contact.email, function(code) {
				var index = -1;
                for (var i in $scope.contacts) {
                    if ($scope.contacts[i].email == contact.email) {
                        index = i;
                        break;
                    }
                }
                if (index != -1) {
                    $scope.contacts.splice(i, 1);
                }
			});
        };
      
        $scope.contacts = [];
        $scope.expanded = true;
        $scope.filterIsVerified = false;
        $scope.loadContacts();
    }]);