#!/bin/sh
kubectl apply -f ./database-deployment.yml
kubectl apply -f ./database-service.yml
kubectl apply -f ./backend-deployment.yml
kubectl apply -f ./backend-service.yml
kubectl apply -f ./frontend-deployment.yml
kubectl apply -f ./frontend-service.yml